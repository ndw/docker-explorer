;;; docker-explorer.el --- -*- lexical-binding: t -*-

;; Copyright © 2020 Norman Walsh

;; Author: Norman Walsh <ndw@nwalsh.com>
;; Version: 0.0.2
;; Keywords: docker, transient

;; This file is not part of GNU Emacs.

;; This program is Free Software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
;; or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
;; for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This file provides a `docker-explorer` mode that lists containers.
;; From the container list, you can hit 'l' to list the MarkLogic log
;; files in the container. Within the log file list, 't' will start
;; the tail menu.

;; Within the transient tail menu after you hit 't':

;; -f lets you specify that you want the process to continue running,
;; adding whatever gets output to the buffer.

;; -n lets you specify the number of lines you'd like in the tail.
;; -f and -n are probably incompatible and I should figure out how to
;; identify that in transient.

;;; History

;; 0.0.2, 4 March 2020
;;   Changed so that "l" lists log files and "t" there shows tail
;; 0.0.1, 3 March 2020
;;   Initial relase

;;; Code:

(require 'transient)

(defconst dxp-buffer "*Docker Explorer*"
  "The name of the docker explorer buffer.")

(defconst dxp-logs-buffer "*Docker Logs Explorer*"
  "The name of the docker logs explorer buffer.")

(defconst dxp-headers '(("ID" 28) ("Names" 20) ("Image" 35) ("Status" 20))
  "The names and widths of the columns to display.
Column names are docker ps format strings.")

;;;###autoload
(defun docker-explorer ()
  "Command to start docker-explorer-mode."
  (interactive)
  (switch-to-buffer dxp-buffer)
  (docker-explorer-mode))

;; ======================================================================

(defun dxp--format ()
  "Return the docker ps format string.
The format string always begins with the ID because we need that
for subsequent commands."
  (let ((format "{{.ID}}")
        (hlist dxp-headers))
    (while hlist
      (setq format (concat format ";{{." (caar hlist) "}}"))
      (setq hlist (cdr hlist)))
    format))

(defun dxp--rows ()
  "Return the output of docker ps formatted as table rows.
The rows must be formatted for `tabulated-list-mode`."
  (let* ((command (format "docker ps -a --format=\"%s\"" (dxp--format)))
         (containers (split-string (shell-command-to-string command) "\n"))
         (rows nil))
    (while containers
      (let ((row (split-string (car containers) ";")))
        (if (not (string= (car containers) ""))
            (setq rows (nconc rows (list (list (car row) (vconcat [] (cdr row)))))))
        (setq containers (cdr containers))))
    rows))

(defun dxp--container-status (container)
  "Return the status of the CONTAINER."
  (let* ((command (format "docker ps -a --format=\"{{.ID}};{{.Status}}\""))
         (containers (split-string (shell-command-to-string command) "\n"))
         (status nil))
    (while containers
      (let ((row (split-string (car containers) ";")))
        (if (string= (car row) container)
            (setq status (cadr row)))
        (setq containers (cdr containers))))
    status))

(defvar docker-explorer-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "l") 'dxp--logs-command)
    map)
  "Keymap for the docker-explorer-mode.")

(define-derived-mode docker-explorer-mode tabulated-list-mode "Docker Explorer"
  "Docker explorer mode."
  (let ((columns (vconcat [] dxp-headers)))
    (setq tabulated-list-format columns)
    (setq tabulated-list-entries (dxp--rows))
    (tabulated-list-init-header)
    (tabulated-list-print)))

(defun dxp--tail-ml-logs (container &optional port args)
  "Tail a MarkLogic server log file on CONTAINER.
If PORT is specified, tail the application error log for that port,
otherwise tail the system error log. If additional ARGS are specified,
pass them along to tail."
  (let ((logfile (if port
                     (format "/var/opt/MarkLogic/Logs/%s_ErrorLog.txt" port)
                   "/var/opt/MarkLogic/Logs/ErrorLog.txt")))
    (dxp--tail-file container logfile args)))

(defun dxp--tail-file (container logfile &optional args)
  "Tail the log file LOGFILE on CONTAINER.
If additional ARGS are specified, pass them along to tail."
  (let ((process (format "*dpx-tail-%s-%s*" container logfile))
        (buffer  (format "*dpx-tail-%s-%s*" container logfile))
        (follow  (or (member "-f" args) (member "-F" args)))
        (command (format "tail %s %s" (string-join args " ") logfile)))
    (if follow
        (start-process process buffer "docker" "exec" container
                       "/bin/bash" "-c" command)
      (call-process "docker" nil buffer nil "exec" container
                    "/bin/bash" "-c" command))
    (switch-to-buffer buffer)))

(defun dxp--tail-ml-function (&optional args)
  "The tail function called by transient.
ARGS are computed by transient interactions with the user.
Note that the port argument (-p) is a bit of a lie, it's not
actually for tail, we're just tunneling the port number.
There's probably a better way."
  (interactive
   (list (transient-args 'dxp--transient-tail-ml)))
  (let* ((container (car (split-string (tabulated-list-get-id) "/")))
        (logfile (concat "/var/opt/MarkLogic/Logs/" (aref (tabulated-list-get-entry) 0)))
        (rest nil))
    (while args
      (setq rest (nconc rest (list (car args))))
      (setq args (cdr args)))
    (dxp--tail-file container logfile rest)))

(define-infix-argument dxp-transient:--lines ()
  "Transient support for the lines argument."
  :description "Number of lines"
  :class 'transient-option
  :shortarg "-n"
  :argument "-n ")

(define-transient-command dxp--transient-tail-ml ()
  "Docker Explorer Tail command"
  ["Arguments"
   ("-f" "Follow" "-f")
   (dxp-transient:--lines)]
  ["Actions"
   ("t" "Tail" dxp--tail-ml-function)])

;; ======================================================================

(defun dxp--tail-command ()
  "Run the tabular command on the container identified by a particular row.
If the container on that row isn't running, just output a message."
  (interactive)
  (dxp--transient-tail-ml))

(defvar docker-logs-explorer-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "t") 'dxp--tail-command)
    map)
  "Keymap for the docker-logs-explorer-mode.")

(define-derived-mode docker-logs-explorer-mode tabulated-list-mode "Docker Logs Explorer"
  "Docker logs explorer mode." t)

(defun docker-logs-explorer (container)
  "Command to start docker-logs-explorer-mode.
The logs in CONTAINER are displayed."
  (interactive)
  (switch-to-buffer dxp-logs-buffer)
  (erase-buffer)
  (docker-logs-explorer-mode)
  (let ((columns (vector '("Filename" 25) '("Size" 25) '("Date" 25))))
    (setq tabulated-list-format columns)
    (setq tabulated-list-entries (dxp--marklogic-logs container))
    (message "")
    (tabulated-list-init-header)
    (tabulated-list-print)))

(defun dxp--logs-command ()
  "Run the tabular command on the container identified by a particular row.
If the container on that row isn't running, just output a message."
  (interactive)
  (let* ((container (aref (tabulated-list-get-entry) 0))
         (status (dxp--container-status container)))
    (if (string= (substring status 0 2) "Up")
        (progn
          (message "Reading list of log files...")
          (docker-logs-explorer container))
      (message "Container is not running."))))

(defun dxp--marklogic-logs (container &optional include-empty)
  "Get the table of MarkLogic log files available in CONTAINER.
If INCLUDE-EMPTY is true, empty log files will be included,
otherwise they're suppressed."
  (with-temp-buffer
    (let ((rows nil))
      (call-process "docker" nil (current-buffer) nil "exec" container
                    "/bin/bash" "-c" "ls -lACo /var/opt/MarkLogic/Logs")
      (goto-char (point-min))
      (if (looking-at "total")
          (delete-region
           (line-beginning-position)
           (1+ (line-end-position))))
      (while (< (point) (point-max))
        ;; -rw-r--r-- 1 docker-user     0 Mar  4 08:14 7997_AccessLog.txt
        ;; Skip -rw-r--r-- 1 docker-user
        (re-search-forward "[[:space:]]+[[:digit:]]+[[:space:]]+[^[:space:]]+")
        ;; Match      0 Mar  4 08:14 7997_AccessLog.txt
        (if (looking-at "\\([[:space:]]+[[:digit:]]+\\)[[:space:]]\\(.*\\)[[:space:]]\\([^[:space:]]+$\\)")
            (let ((row (list (format "%s/%s" container (match-string 3))
                             (vector (match-string 3) (match-string 1) (match-string 2)))))
              (if (or include-empty (> (string-to-number (match-string 1)) 0))
                  (setq rows (nconc rows (list row))))))
        (beginning-of-line)
        (next-line))
      rows)))

(provide 'docker-explorer)

;;; docker-explorer ends here
